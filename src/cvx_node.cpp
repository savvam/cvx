/**
 * @file cvx_node.cpp
 * @brief Entry point for ROS cvx trajectory generator
 * @date 18 January 2020
 */

#include <ros/ros.h>

#include "cvx/cvx_ros.h"

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "cvx");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::cvx::CVX_ROS cvx(nhtopics, nhparams);
  ros::spin();
  return 0;
}
